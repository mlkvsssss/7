#include <Functions.hpp>
#include <iostream>
using namespace std;

void in(int a[100][100], int n)
{
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
        {
            cin >> a[i][j];
        }
}

void sort(int a[100][100], int n)
{
    int p = 1;
    int t = 0;
    int g = 0;
    int b[100];
    for (int i = 0; i < n; i++)
    {
        b[i] = a[i][i];
        p *= a[i][i];
    }
    cout << endl;
    for (int i = 0; i < n; i++)
    {
        t = 0;
        for (int j = 0; j < n; j++)
        {
            if (b[i] > a[i][j]) { t += 1; }
        }
        if (t == n - 1) { g += 1; }
    }
    if (g == n)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (a[i][j] == 0) { a[i][j] = p; }
            }
        }
    }
}

void out(int a[100][100], int n)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }
}
